package com.abogadosdeldiablo.application.service;

import java.util.List;

import com.abogadosdeldiablo.application.model.Law;
import com.abogadosdeldiablo.application.model.dto.LawDTO;

public interface LawService {
	
	public Law createLaw(Law law);
	
	public Law getById(Long id);
	
	public List<LawDTO> getAllLaws();

}
