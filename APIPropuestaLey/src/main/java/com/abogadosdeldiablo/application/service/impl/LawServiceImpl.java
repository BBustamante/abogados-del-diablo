package com.abogadosdeldiablo.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abogadosdeldiablo.application.model.Law;
import com.abogadosdeldiablo.application.model.LawProposal;
import com.abogadosdeldiablo.application.model.dto.LawDTO;
import com.abogadosdeldiablo.application.repository.LawRepository;
import com.abogadosdeldiablo.application.service.LawService;

@Service
public class LawServiceImpl implements LawService {

	@Autowired
	private LawRepository lawRepository;

	@Override
	public Law createLaw(Law law) {
		return lawRepository.save(law);
	}
	

	@Override
	public Law getById(Long id) {
		return lawRepository.getOne(id);
	}

	@Override
	public List<LawDTO> getAllLaws() {
		List<Law> laws = lawRepository.findAll();
		List<LawDTO> lawsDTO = new ArrayList<LawDTO>();
		for (Law law : laws) {
			lawsDTO.add(getLawDTO(law));
		}
		return lawsDTO;
	}

	private LawDTO getLawDTO(Law law) {
		LawDTO lawDTO = new LawDTO();
		lawDTO.setLawId(law.getId());
		lawDTO.setLawStatusType(law.getStatus());
		lawDTO.setNumberOfVotes(law.getNumberOfVotes());
		lawDTO.setNumberOfValorations(law.getNumberOfValorations());
		lawDTO.setVotesValueResult(law.getVotesValueResult());
		lawDTO.setRatingAverage(law.getRatingAverage());
		LawProposal lawProposal = getFirstLawProposal(law.getLawProposals());
		lawDTO.setLawProposalTitle(lawProposal.getTitle());
		lawDTO.setLawProposalDate(lawProposal.getCreationDate());
		return lawDTO;
	}

	private LawProposal getFirstLawProposal(List<LawProposal> lawProposals) {
		if (lawProposals.size() > 0) {
			return lawProposals.get(0);
		} else {
			return new LawProposal();
		}
	}

}
