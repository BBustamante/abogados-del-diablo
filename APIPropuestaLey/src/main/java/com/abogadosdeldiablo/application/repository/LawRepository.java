package com.abogadosdeldiablo.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abogadosdeldiablo.application.model.Law;

public interface LawRepository extends JpaRepository<Law, Long>{


}
