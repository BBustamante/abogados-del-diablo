package com.abogadosdeldiablo.application.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abogadosdeldiablo.application.model.Law;
import com.abogadosdeldiablo.application.model.dto.LawDTO;
import com.abogadosdeldiablo.application.service.LawService;

@RestController
@CrossOrigin(origins = "*")
public class LawController {

	@Autowired
	private LawService lawService;
	
	@PostMapping("/law/create")
	public Law createLaw1(@RequestBody Law law) {
		return lawService.createLaw(law);
	}
	
	@PostMapping("/law/get-by-id")
	public Law getById(@RequestParam String id) {
		return lawService.getById(Long.parseLong(id));
	}
	
	@PostMapping("/law/all-laws")
	public List<LawDTO> getAllLaws() {
		return lawService.getAllLaws();
	}

}
