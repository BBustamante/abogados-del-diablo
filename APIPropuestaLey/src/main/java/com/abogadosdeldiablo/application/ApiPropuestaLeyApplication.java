package com.abogadosdeldiablo.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.abogadosdeldiablo")
public class ApiPropuestaLeyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPropuestaLeyApplication.class, args);
	}

}
