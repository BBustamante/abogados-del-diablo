package com.abogadosdeldiablo.application.model.enumerations;

public enum UserType {

	ADMIN,
	PROFESIONAL,
	VISITANT
	
}
