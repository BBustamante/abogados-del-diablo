package com.abogadosdeldiablo.application.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.abogadosdeldiablo.application.model.auxiliar.AmendmentRequest;
import com.abogadosdeldiablo.application.model.auxiliar.Comment;
import com.abogadosdeldiablo.application.model.auxiliar.Suggestion;
import com.abogadosdeldiablo.application.model.auxiliar.Valoration;
import com.abogadosdeldiablo.application.model.auxiliar.Vote;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "ARTICLE")
@Data
public class Article {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "ORDEN")
	private Integer orden;
	
	@Column(name = "TEXTO")
	private String texto;
	
	@Column(name = "VERSION")
	private String version;
	
	@JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Article parent;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private LawProposal lawProposal;
    
    @OneToMany(mappedBy="parent", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Article> children;
    
    @JsonIgnore
    @OneToMany(mappedBy="article", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<AmendmentRequest> amendmentRequests;
    
    @OneToMany(mappedBy="article", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Comment> comments;
    
    @OneToMany(mappedBy="articleSuggestion", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Suggestion> suggestions;
    
    @OneToMany(mappedBy="articleVote", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Vote> votes;
    
    @OneToMany(mappedBy="articleValoration", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Valoration> valoration;

}
