package com.abogadosdeldiablo.application.model.dto;

import java.util.Date;

import com.abogadosdeldiablo.application.model.enumerations.StatusType;
import com.abogadosdeldiablo.application.model.enumerations.ValorationValue;

import lombok.Data;

@Data
public class LawDTO {

	private Long lawId;
	private String lawProposalTitle;
	private Date lawProposalDate;
	private StatusType lawStatusType;
	private Integer numberOfVotes;
	private Boolean votesValueResult;
	private Integer numberOfValorations;
	private ValorationValue ratingAverage;
	
}
