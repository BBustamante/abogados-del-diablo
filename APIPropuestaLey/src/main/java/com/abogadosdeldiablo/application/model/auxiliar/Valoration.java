package com.abogadosdeldiablo.application.model.auxiliar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.abogadosdeldiablo.application.model.Amendment;
import com.abogadosdeldiablo.application.model.Law;
import com.abogadosdeldiablo.application.model.User;
import com.abogadosdeldiablo.application.model.enumerations.ValorationValue;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "VALORATION")
@Data
public class Valoration {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
    @ManyToOne 
    private User owner;
	
	@Column(name = "VALORATION_VALUE")
	private ValorationValue valorationValue;
	
	@JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Law lawProposalValoration;
    
	@JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Amendment amendmentValoration;
    
	@JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Amendment articleValoration;
    
	@JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Amendment dispositionValoration;
	
}
