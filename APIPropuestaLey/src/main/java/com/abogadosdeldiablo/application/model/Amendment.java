package com.abogadosdeldiablo.application.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.abogadosdeldiablo.application.model.auxiliar.AmendmentRequest;
import com.abogadosdeldiablo.application.model.auxiliar.Comment;
import com.abogadosdeldiablo.application.model.auxiliar.Suggestion;
import com.abogadosdeldiablo.application.model.auxiliar.Valoration;
import com.abogadosdeldiablo.application.model.auxiliar.Vote;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "AMENDMENT")
@Data
public class Amendment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
    @OneToMany(mappedBy="amendment", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<AmendmentRequest> amendmentRequests;
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private User owner;
    
    @OneToMany(mappedBy="amendment", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Comment> comments;
    
    @OneToMany(mappedBy="amendmentSuggestion", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Suggestion> suggestions;
    
    @OneToMany(mappedBy="amendmentVote", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Vote> votes;
    
    @OneToMany(mappedBy="amendmentValoration", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Valoration> valoration;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private LawProposal lawProposal;

}
