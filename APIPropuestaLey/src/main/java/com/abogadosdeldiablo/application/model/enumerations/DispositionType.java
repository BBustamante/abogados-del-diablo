package com.abogadosdeldiablo.application.model.enumerations;

public enum DispositionType {
	
	ADITIONAL,
	TRANSITIONAL,
	FINAL,
	DEROGATORY

}
