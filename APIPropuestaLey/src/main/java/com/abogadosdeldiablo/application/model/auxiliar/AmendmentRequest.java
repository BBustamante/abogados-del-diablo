package com.abogadosdeldiablo.application.model.auxiliar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.abogadosdeldiablo.application.model.Amendment;
import com.abogadosdeldiablo.application.model.Article;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "AMENDMENT_REQUEST")
@Data
public class AmendmentRequest {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Article article;
    
    @Column(name = "CHANGE_LOG")
    private String changeLog;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Amendment amendment;
}
