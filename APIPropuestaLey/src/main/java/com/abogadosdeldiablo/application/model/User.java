package com.abogadosdeldiablo.application.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.abogadosdeldiablo.application.model.auxiliar.Comment;
import com.abogadosdeldiablo.application.model.auxiliar.Valoration;
import com.abogadosdeldiablo.application.model.auxiliar.Vote;
import com.abogadosdeldiablo.application.model.enumerations.UserType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "USER")
@Data
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "USER_TYPE")
	private UserType userType;
	
	@JsonIgnore
    @OneToMany(mappedBy="owner", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Comment> comment;
    
	@JsonIgnore
    @OneToMany(mappedBy="owner", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Valoration> valorations;
    
	@JsonIgnore
    @OneToMany(mappedBy="owner", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Vote> votes;
    
	@JsonIgnore
    @OneToMany(mappedBy="owner", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Amendment> amendments;
    
	@JsonIgnore
    @OneToMany(mappedBy="owner", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<LawProposal> lawPrroposals;
	
}
