package com.abogadosdeldiablo.application.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.abogadosdeldiablo.application.model.auxiliar.Comment;
import com.abogadosdeldiablo.application.model.auxiliar.Suggestion;
import com.abogadosdeldiablo.application.model.auxiliar.Valoration;
import com.abogadosdeldiablo.application.model.auxiliar.Vote;
import com.abogadosdeldiablo.application.model.enumerations.StatusType;
import com.abogadosdeldiablo.application.model.enumerations.ValorationValue;

import lombok.Data;

@Entity
@Table(name = "LAW")
@Data
public class Law {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "LAW_ID")
	private Long id;
	
	@Column(name = "STATUS")
	private StatusType status;
	
	@Column(name = "NUMBER_OF_VOTES")
	private Integer numberOfVotes;
	
	@Column(name = "VOTES_VALUE_RESULT")
	private Boolean votesValueResult;
	
	@Column(name = "NUMBER_OF_VALORATIONS")
	private Integer numberOfValorations;
	
	@Column(name = "RATING_AVERAGE")
	private ValorationValue ratingAverage;
	
	@Column(name = "NUMBER_OF_PROPOSALS")
	private Integer numberOfProposals;
	
	@OrderBy("creationDate DESC")
    @OneToMany(mappedBy="law", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<LawProposal> lawProposals;
    
    @OneToMany(mappedBy="lawProposal", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Comment> comments;
    
    @OneToMany(mappedBy="lawProposalSuggestion", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Suggestion> suggestions;
    
    @OneToMany(mappedBy="lawProposalVote", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Vote> votes;
    
    @OneToMany(mappedBy="lawProposalValoration", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Valoration> valoration;
    
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "LAW_DISPOSITION", 
    joinColumns = { @JoinColumn(name = "LAW_ID") }, 
    inverseJoinColumns = { @JoinColumn(name = "DISPOSITION_ID") })
    private List<Disposition> dispositions;
    
}
