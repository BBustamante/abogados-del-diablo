package com.abogadosdeldiablo.application.model.enumerations;

public enum StatusType {

	PENDING_APPROVAL,
	APPROVED,
	CANCELED
	
}
