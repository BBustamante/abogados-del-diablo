package com.abogadosdeldiablo.application.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.abogadosdeldiablo.application.model.auxiliar.Comment;
import com.abogadosdeldiablo.application.model.auxiliar.Suggestion;
import com.abogadosdeldiablo.application.model.auxiliar.Valoration;
import com.abogadosdeldiablo.application.model.auxiliar.Vote;
import com.abogadosdeldiablo.application.model.enumerations.DispositionType;

import lombok.Data;

@Entity
@Table(name = "DISPOSITION")
@Data
public class Disposition {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DISPOSITION_ID")
	private Long id;
	
	@Column(name = "TEXT")
	private String text;
	
	@Column(name = "DISPOSITION_TYPE")
	private DispositionType dispositionType;
	
    @OneToMany(mappedBy="disposition", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Comment> comments;
    
    @OneToMany(mappedBy="dispositionSuggestion", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Suggestion> suggestions;
    
    @OneToMany(mappedBy="dispositionVote", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Vote> votes;
    
    @OneToMany(mappedBy="dispositionValoration", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Valoration> valoration;

}
