package com.abogadosdeldiablo.application.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.abogadosdeldiablo.application.model.enumerations.ValorationValue;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "LAW_PROPOSAL")
@Data
public class LawProposal {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "LAW_PROPOSAL_ID")
	private Long id;
	
	@Column(name = "TITLE")
	private String title;
	
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private User owner;
    
    @Column(name = "EXPOSITION_GROUNDS")
    private String expositionGrounds;
    
    @Column(name = "PREAMBLE")
    private String preamble;
    
    @Column(name = "CREATION_DATE")
    private Date creationDate;
    
	@Column(name = "NUMBER_OF_VOTES")
	private Integer numberOfVotes;
	
	@Column(name = "VOTES_VALUE_RESULT")
	private Boolean votesValueResult;
	
	@Column(name = "NUMBER_OF_VALORATIONS")
	private Integer numberOfValorations;
	
	@Column(name = "RATING_AVERAGE")
	private ValorationValue ratingAverage;
    
    @OrderBy("orden ASC")
    @OneToMany(mappedBy="lawProposal", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Article> articles;
    
    @OneToMany(mappedBy="lawProposal", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private List<Amendment> amendments;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Law law;
    
}
