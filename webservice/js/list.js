function listLaws( data ) {
  $.each(data, function(i, obj) {
    card = document.createElement('div');
    $(card).addClass('card col-xs-12 col-md-4');

    cardBody = document.createElement('div');
    $(cardBody).addClass('card-body wrapper fadeInDown');

    cardContainer = document.createElement('div');
    $(cardContainer).addClass('card-container');

    cardTitle = document.createElement('h5');
    $(cardTitle).addClass('card-title');
    $(cardTitle).html(obj.lawProposalTitle);
    $(cardContainer).append(cardTitle);

    cardSubtitle = document.createElement('h6');
    switch (obj.lawStatusType) {
      case 'CANCELED':
        $(cardSubtitle).addClass('card-subtitle mb-2 text-danger');
        break;
      case 'APPROVED':
        $(cardSubtitle).addClass('card-subtitle mb-2 text-success');
        break;
      default:
        $(cardSubtitle).addClass('card-subtitle mb-2 text-muted');
        break;
    }
    $(cardSubtitle).html(obj.lawStatusType);
    $(cardContainer).append(cardSubtitle);

    votes = document.createElement('div');
    if(obj.votesValueResult) {
      $(votes).addClass('glyphicon glyphicon-thumbs-up');
    } else {
      $(votes).addClass('glyphicon glyphicon-thumbs-down');
    }
    votes.setAttribute('data-toggle','tooltip');
    votes.setAttribute('data-placement','right');
    votes.setAttribute('data-title',obj.numberOfVotes);
    $(cardContainer).append(votes);

    average = obj.ratingAverage;
    s = document.createElement('div');
    s1 = document.createElement('span');
    s2 = document.createElement('span');
    s3 = document.createElement('span');
    s4 = document.createElement('span');
    s5 = document.createElement('span');
    switch (average) {
      case 'EXCELENT':
        $(s1).addClass('glyphicon glyphicon-star');
        $(s2).addClass('glyphicon glyphicon-star');
        $(s3).addClass('glyphicon glyphicon-star');
        $(s4).addClass('glyphicon glyphicon-star');
        $(s5).addClass('glyphicon glyphicon-star');
        break;
      case 'VERY_GOOD':
        $(s1).addClass('glyphicon glyphicon-star');
        $(s2).addClass('glyphicon glyphicon-star');
        $(s3).addClass('glyphicon glyphicon-star');
        $(s4).addClass('glyphicon glyphicon-star');
        $(s5).addClass('glyphicon glyphicon-star-empty');
        break;
      case 'GOOD':
        $(s1).addClass('glyphicon glyphicon-star');
        $(s2).addClass('glyphicon glyphicon-star');
        $(s3).addClass('glyphicon glyphicon-star');
        $(s4).addClass('glyphicon glyphicon-star-empty');
        $(s5).addClass('glyphicon glyphicon-star-empty');
        break;
      case 'AVERAGE':
        $(s1).addClass('glyphicon glyphicon-star');
        $(s2).addClass('glyphicon glyphicon-star');
        $(s3).addClass('glyphicon glyphicon-star-empty');
        $(s4).addClass('glyphicon glyphicon-star-empty');
        $(s5).addClass('glyphicon glyphicon-star-empty');
        break;
      case 'POOR':
        $(s1).addClass('glyphicon glyphicon-star');
        $(s2).addClass('glyphicon glyphicon-star-empty');
        $(s3).addClass('glyphicon glyphicon-star-empty');
        $(s4).addClass('glyphicon glyphicon-star-empty');
        $(s5).addClass('glyphicon glyphicon-star-empty');
        break;
      default:
        $(s1).addClass('glyphicon glyphicon-star-empty');
        $(s2).addClass('glyphicon glyphicon-star-empty');
        $(s3).addClass('glyphicon glyphicon-star-empty');
        $(s4).addClass('glyphicon glyphicon-star-empty');
        $(s5).addClass('glyphicon glyphicon-star-empty');
        break;
    }
    s.setAttribute('data-toggle','tooltip');
    s.setAttribute('data-placement','right');
    s.setAttribute('data-title',obj.numberOfValorations);
    $(s).append(s1);
    $(s).append(s2);
    $(s).append(s3);
    $(s).append(s4);
    $(s).append(s5);
    $(cardContainer).append(s);

    $(cardBody).append(cardContainer);

    $(cardBody).bind('click', function() {
      location.href = 'http://' + window.location.host + '?path=law&id=' + obj.lawId;
    });

    $(card).append(cardBody);
    $('#law-cards').append(card);
  });
  $('[data-toggle="tooltip"]').tooltip();
}
