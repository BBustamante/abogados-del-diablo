function openNav() {
  document.getElementById("myNav").style.width = "100%";
}
function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}

$( document ).ready(function() {
  $('#addArticle').click( function() {
    $('#addArticleContainer').html( $('#addArticleContainer').html() + $('#addArticleRef').html());
  });
  $( document ).on('click', '.dellArticle', function() {
    $(this).closest('.row').remove();
  });

  $('#addDisp').click( function() {
    $('#addDispContainer').html( $('#addDispContainer').html() + $('#addDispRef').html());
  });
  $( document ).on('click', '.dellDisp', function() {
    $(this).closest('.row').remove();
  });

  if($("#law-cards").length) {
    $.ajax({
      url: 'http://localhost:8080/law/all-laws',
      method: "POST",
      dataType: "json",
    }).done( function(data) {
      listLaws( data );
    });
  }

  if($("#law-post").length) {
    $.ajax({
      url: 'http://localhost:8080/law/get-by-id',
      data: {'id':$('#law-post').data('id')},
      method: "POST",
      dataType: "json",
    }).done( function(data) {
      showLaw( data );
    });
  }
});
