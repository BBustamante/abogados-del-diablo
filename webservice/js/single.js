function showLaw( data ) {
  numprop = data.numberOfProposals;
  currentprop = $('#law-post').data('proposal');
  btns = document.createElement('div');
  for (var i = 0; i < numprop; i++) {
    btn = document.createElement('button');
    if(i==currentprop){
      $(btn).addClass('btn btn-secondary');
    } else {
      $(btn).addClass('btn btn-primary');
      btn.setAttribute('data-id',i);
      $(btn).bind('click', function() {
        location.href = 'http://' + window.location.host + '?path=law&id=' + data.id + '&proposal=' + $(this).data('id');
      });
    }
    $(btn).html(i+1);
    btns.append(btn);
  }

  h1 = document.createElement('h1');
  $(h1).html(data.lawProposals[currentprop].title);

  let comments = data.comments.length;
  if( comments > 0 ) {
    n = document.createElement('span');
    $(n).addClass('badge badge-secondary');
    $(n).html(data.comments.length);

    i = document.createElement('span');
    $(i).addClass('glyphicon glyphicon-comment');

    b = document.createElement('button');
    $(b).addClass('btn btn-primary');
    $(b).append(i);
    $(b).append(' ');
    $(b).append(n);
    $(h1).append(b);
  }

  let suggestions = data.suggestions.length;
  if( suggestions > 0 ) {
    n = document.createElement('span');
    $(n).addClass('badge badge-secondary');
    $(n).html(data.suggestions.length);

    i = document.createElement('span');
    $(i).addClass('glyphicon glyphicon-question-sign');

    b = document.createElement('button');
    $(b).addClass('btn btn-primary');
    $(b).append(i);
    $(b).append(' ');
    $(b).append(n);
    $(h1).append(b);
  }
  $('#law-post').append(h1);
  $('#law-post').append(btns);

  p = document.createElement('p');
  el = document.createElement('i');
  $(el).html(data.lawProposals[currentprop].expositionGrounds);
  $(p).append(el);
  $('#law-post').append(p);

  p = document.createElement('p');
  el = document.createElement('i');
  $(el).html(data.lawProposals[currentprop].preamble);
  $(p).append(el);
  $('#law-post').append(p);

  let articles = data.lawProposals[currentprop].articles.length;
  if( articles > 0 ) {
    h2 = document.createElement('h2');
    $(h2).html('Artículos');
    $('#law-post').append(h2);

    $.each(data.lawProposals[currentprop].articles, function(i, article) {
      p = document.createElement('p');
      b = document.createElement('b');
      el = document.createElement('span');
      $(el).html(article.orden);
      $(b).append(el);

      if( article.version ) {
        el = document.createElement('span');
        $(el).html(' - ' + article.version);
        $(b).append(el);
      }

      $(p).append(b);
      $('#law-post').append(p);

      el = document.createElement('p');
      $(el).html(article.texto);
      $('#law-post').append(el);

      let comments = article.comments.length;
      if( comments > 0 ) {
        n = document.createElement('span');
        $(n).addClass('badge badge-secondary');
        $(n).html(article.comments.length);

        i = document.createElement('span');
        $(i).addClass('glyphicon glyphicon-comment');

        b = document.createElement('button');
        $(b).addClass('btn btn-primary');
        $(b).append(i);
        $(b).append(' ');
        $(b).append(n);
        $('#law-post').append(b);
      }

      let suggestions = article.suggestions.length;
      if( suggestions > 0 ) {
        n = document.createElement('span');
        $(n).addClass('badge badge-secondary');
        $(n).html(article.suggestions.length);

        i = document.createElement('span');
        $(i).addClass('glyphicon glyphicon-question-sign');

        b = document.createElement('button');
        $(b).addClass('btn btn-primary');
        $(b).append(i);
        $(b).append(' ');
        $(b).append(n);
        $('#law-post').append(b);
      }

      c = document.createElement('div');
      votes = document.createElement('span');
      if(data.lawProposals[currentprop].votesValueResult) {
        $(votes).addClass('glyphicon glyphicon-thumbs-up');
      } else {
        $(votes).addClass('glyphicon glyphicon-thumbs-down');
      }
      votes.setAttribute('data-toggle','tooltip');
      votes.setAttribute('data-placement','right');
      votes.setAttribute('data-title',data.lawProposals[currentprop].numberOfVotes);
      $(c).append(votes);

      average = data.lawProposals[currentprop].ratingAverage;
      s = document.createElement('span');
      s1 = document.createElement('span');
      s2 = document.createElement('span');
      s3 = document.createElement('span');
      s4 = document.createElement('span');
      s5 = document.createElement('span');
      switch (average) {
        case 'EXCELENT':
          $(s1).addClass('glyphicon glyphicon-star');
          $(s2).addClass('glyphicon glyphicon-star');
          $(s3).addClass('glyphicon glyphicon-star');
          $(s4).addClass('glyphicon glyphicon-star');
          $(s5).addClass('glyphicon glyphicon-star');
          break;
        case 'VERY_GOOD':
          $(s1).addClass('glyphicon glyphicon-star');
          $(s2).addClass('glyphicon glyphicon-star');
          $(s3).addClass('glyphicon glyphicon-star');
          $(s4).addClass('glyphicon glyphicon-star');
          $(s5).addClass('glyphicon glyphicon-star-empty');
          break;
        case 'GOOD':
          $(s1).addClass('glyphicon glyphicon-star');
          $(s2).addClass('glyphicon glyphicon-star');
          $(s3).addClass('glyphicon glyphicon-star');
          $(s4).addClass('glyphicon glyphicon-star-empty');
          $(s5).addClass('glyphicon glyphicon-star-empty');
          break;
        case 'AVERAGE':
          $(s1).addClass('glyphicon glyphicon-star');
          $(s2).addClass('glyphicon glyphicon-star');
          $(s3).addClass('glyphicon glyphicon-star-empty');
          $(s4).addClass('glyphicon glyphicon-star-empty');
          $(s5).addClass('glyphicon glyphicon-star-empty');
          break;
        case 'POOR':
          $(s1).addClass('glyphicon glyphicon-star');
          $(s2).addClass('glyphicon glyphicon-star-empty');
          $(s3).addClass('glyphicon glyphicon-star-empty');
          $(s4).addClass('glyphicon glyphicon-star-empty');
          $(s5).addClass('glyphicon glyphicon-star-empty');
          break;
        default:
          $(s1).addClass('glyphicon glyphicon-star-empty');
          $(s2).addClass('glyphicon glyphicon-star-empty');
          $(s3).addClass('glyphicon glyphicon-star-empty');
          $(s4).addClass('glyphicon glyphicon-star-empty');
          $(s5).addClass('glyphicon glyphicon-star-empty');
          break;
      }
      s.setAttribute('data-toggle','tooltip');
      s.setAttribute('data-placement','right');
      s.setAttribute('data-title',data.lawProposals[currentprop].numberOfValorations);
      $(s).append(s1);
      $(s).append(s2);
      $(s).append(s3);
      $(s).append(s4);
      $(s).append(s5);
      $(c).append(s);
      $('#law-post').append(c);

      el = document.createElement('hr');
      $('#law-post').append(el);
    });

    let dispositions = data.dispositions.length;
    if( dispositions > 0 ) {
      h2 = document.createElement('h2');
      $(h2).html('Disposiciones');
      $('#law-post').append(h2);
      $.each(data.dispositions, function(i, disposition) {
        el = document.createElement('div');

        b = document.createElement('b');
        $(b).html(disposition.dispositionType);
        $(el).append(b);

        t = document.createElement('div');
        $(t).html(disposition.text);
        $(el).append(t);

        $('#law-post').append(el);

        let comments = disposition.comments.length;
        if( comments > 0 ) {
          n = document.createElement('span');
          $(n).addClass('badge badge-secondary');
          $(n).html(disposition.comments.length);

          i = document.createElement('span');
          $(i).addClass('glyphicon glyphicon-comment');

          b = document.createElement('button');
          $(b).addClass('btn btn-primary');
          $(b).append(i);
          $(b).append(' ');
          $(b).append(n);
          $('#law-post').append(b);
        }

        let suggestions = disposition.suggestions.length;
        if( suggestions > 0 ) {
          n = document.createElement('span');
          $(n).addClass('badge badge-secondary');
          $(n).html(disposition.suggestions.length);

          i = document.createElement('span');
          $(i).addClass('glyphicon glyphicon-question-sign');

          b = document.createElement('button');
          $(b).addClass('btn btn-primary');
          $(b).append(i);
          $(b).append(' ');
          $(b).append(n);
          $('#law-post').append(b);
        }

        el = document.createElement('hr');
        $('#law-post').append(el);
      });
    }
  }
  $('[data-toggle="tooltip"]').tooltip();
}
