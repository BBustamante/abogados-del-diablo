<div class="container">
  <form class="large-form">
    <div class="row">
      <div class="col-xs-3"></div>
      <div class="col-xs-6">
        <div class="row">
          <div class="col-xs-12">
            <p class="h4 text-center">Crear propuesta de Ley</p>
          </div>
          <div class="col-xs-12">
            <input type="text" class="form-control" placeholder="Título de la propuesta">
          </div>
          <div class="col-xs-12">
            <textarea class="form-control" placeholder="Exposición de motivos"></textarea>
          </div>
          <div class="col-xs-12">
            <textarea class="form-control" placeholder="Preámbulo"></textarea>
          </div>

          <div class="col-xs-12 col-md-3">
            <input type="text" class="form-control" placeholder="Libro">
          </div>
          <div class="col-xs-12 col-md-3">
            <input type="text" class="form-control" placeholder="Título">
          </div>
          <div class="col-xs-12 col-md-3">
            <input type="text" class="form-control" placeholder="Capítulo">
          </div>
          <div class="col-xs-12 col-md-3">
            <input type="text" class="form-control" placeholder="Sección">
          </div>
          <div class="col-xs-12">
            <div id="addArticleContainer"></div>
            <p id="addArticle"><span class="glyphicon glyphicon-plus"></span> Anadir Artículo</p>
            <div id="addArticleRef">
              <div class="row">
                <div class="col-xs-3"><b>Artículo:</b></div>
                <div class="col-xs-3"><input type="text" class="form-control" placeholder="Orden"></div>
                <div class="col-xs-3"><input type="text" class="form-control" placeholder="Versión"></div>
                <div class="col-xs-3 text-right"><span class="dellArticle glyphicon glyphicon-trash text-danger pointer"></span></div>
                <div class="col-xs-12"><textarea class="form-control" placeholder="Contenido"></textarea></div>
              </div>
            </div>
          </div>
          <div class="col-xs-12">
            <div id="addDispContainer"></div>
            <p id="addDisp"><span class="glyphicon glyphicon-plus"></span> Anadir Disposición</p>
            <div id="addDispRef">
              <div class="row">
                <div class="col-xs-3"><b>Disposición:</b></div>
                <div class="col-xs-3">
                  <select class="form-control">
                    <option>Adicional</option>
                    <option>Derrogatoria</option>
                    <option>Final</option>
                  </select>
                </div>
                  <div class="col-xs-3"></div>
                <div class="col-xs-3 text-right"><span class="dellArticle glyphicon glyphicon-trash text-danger pointer"></span></div>
                <div class="col-xs-12"><textarea class="form-control" placeholder="Contenido"></textarea></div>
              </div>
            </div>
          </div>

          <div class="col-xs-12">
            <button class="btn btn-info btn-block" type="submit">Enviar</button>
          </div>
        </div>
        </div>
      </div>
    </div>
  </form>
</div>
