<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Propuestas de ley</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/custom.css">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet">
    <!-- Last jQuery version -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="/js/bootstrap.min.js"></script>
    <!-- Custom JS -->
    <script src="/js/custom.js"></script>
    <script src="/js/list.js"></script>
    <script src="/js/single.js"></script>
  </head>
  <body>
    <?php
    include('header.php');
    $path = ( isset($_GET['path']) ) ? $_GET['path'] : '';
    switch( $path ) {
      case 'login':
        include('login.php');
        break;
      case 'search':
        include('search.php');
        break;
      case 'create':
        include('create.php');
        break;
      case 'law':
        include('law.php');
        break;
      default:
        include('default.php');
        break;
    }
    include('footer.php');
    ?>
 </body>
</html>
