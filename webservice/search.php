<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Icon -->
    <div class="fadeIn first" style="padding-top:5px;">
      <span class="glyphicon glyphicon-search"></span>
    </div>

    <!-- Search Form -->
    <form action="/" method="post">
      <input type="text" class="form-control" placeholder="Buscar...">
      <div class="row">
        <div class="col-xs-6">
          <label class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input">
            <span class="custom-control-indicator"></span>
            Aprobadas
          </label>
        </div>
        <div class="col-xs-6">
          <label class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input">
            <span class="custom-control-indicator"></span>
            Canceladas
          </label>
        </div>
        <!--end of col-->
      </div>
      <input type="submit" class="fadeIn fourth" value="Buscar">
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
      Propuestas de Ley
    </div>
  </div>
</div>
